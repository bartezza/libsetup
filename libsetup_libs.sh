#!/bin/bash

export LIBRARIES_ALL=( 'amd' 'blacs' 'blas' 'boost' 'cmake' 'curl' 'eigen' 'gcc' 'glm' 'gperftools' 'hdf5' 'lapack' 'lua' 'metis' 'mumps' 'openmpi' 'openssl' 'parmetis' 'scalapack' 'scotch' 'suitesparse' 'tidy' 'trilinos_isoglib' 'umfpack' )

export LIBRARIES_NUM=${#LIBRARIES_ALL[@]}

# source all libraries
function libraries_sourceAll {
    for (( i=0;i<$LIBRARIES_NUM;i++ )); do
        LIB=${LIBRARIES_ALL[${i}]};
        # try with cluster specific library
        TEST="$LIBSETUP_BASE/libs/${LIB}_${CLUSTER_CONFIG}.sh"
        #echo $TEST
        if [ -f "$TEST" ]; then
            # install
            #echo "Sourcing $TEST..."
            source $TEST
        else
            # try with custom library
            TEST="$LIBSETUP_BASE/libs/${LIB}.sh"
            #echo $TEST
            if [ -f "$TEST" ]; then
                # install
                #echo "Sourcing $TEST..."
                source $TEST
            fi
        fi
    done
}

# check if a library is installed and set usefult env vars
# usage: libraries_enable <name> <version>
function libraries_enable {
    BASE_NAME=$1
    VERSION=$2
    if [ "$SEPARATE_INSTALL" != "1" ]; then
        INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
        if [ -f "$INSTALLED" ]; then
            # use custom one
            export CUR_BASE_DIR=$LIBRARIES_PREFIX
            export CUR_INCLUDE_DIR=$LIBRARIES_PREFIX/include
            export CUR_LIB_DIR=$LIBRARIES_PREFIX/lib
            export CUR_BIN_DIR=$LIBRARIES_PREFIX/bin
            echo "Using custom ${BASE_NAME}"
        else
            export CUR_BASE_DIR=
            export CUR_INCLUDE_DIR=
            export CUR_LIB_DIR=
            export CUR_BIN_DIR=
            echo "Using system installed ${BASE_NAME}"
        fi
    else
        INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
        if [ -d "$INSTALL_DIR" ]; then
            # use custom one
            export CUR_BASE_DIR=$INSTALL_DIR
            export CUR_INCLUDE_DIR=$INSTALL_DIR/include
            export CUR_LIB_DIR=$INSTALL_DIR/lib
            export CUR_BIN_DIR=$INSTALL_DIR/bin
            echo "Using custom ${BASE_NAME} from '$INSTALL_DIR'"
        else
            echo "[Error] ${BASE_NAME} not found in '$INSTALL_DIR'"
            exit 1
        fi
    fi
}

# uninstall an installed lib
# usage: libraries_uninstallLib <name> <version>
function libraries_uninstallLib {
    BASE_NAME=$1
    VERSION=$2
    # prompt
    P="[${Cyan}${BASE_NAME}${Color_Off}]"
    # uninstall
    BASE_DIR=${BASE_NAME}-${VERSION}
    if [ "$SEPARATE_INSTALL" != "1" ]; then
        BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
        cd $BUILD_DIR
        echo -e "$P Uninstalling..."
        make uninstall
    else
        INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
        rm -rf $INSTALL_DIR
    fi
    echo -e "$P Uninstalled"
}

# clean package and directory of a lib
# usage: libraries_cleanLib <name> <version>
function libraries_cleanLib {
    BASE_NAME=$1
    VERSION=$2
    # prompt
    P="[${Cyan}${BASE_NAME}${Color_Off}]"
    # remove dir
    BASE_DIR=${BASE_NAME}-${VERSION}
    rm -rf $BASE_DIR
    # remove package
    PACKAGE=${BASE_DIR}.tar.gz
    rm -f $PACKAGE
    # remove build dir
    BUILD_DIR=${BASE_DIR}_Build*
    rm -rf $BUILD_DIR
    echo -e "$P Cleaned"
}

# build cmake define
# usage: res=`cmakeDefine <defineName> <defineValue>`
function cmakeDefine {
    DEFINE_NAME=$1
    DEFINE_VALUE=$2
    #echo "Name = '$DEFINE_NAME', value = '$DEFINE_VALUE'"
    if [ "$DEFINE_VALUE" != "" ]; then
        echo "-D ${DEFINE_NAME}=${DEFINE_VALUE}"
    fi
}

# callFunc <opcode> <func>
function callFunc {
    OPCODE=$1
    FUNC=$2
    #MACHINE_CONFIG <= global variable
    SELECTED_CONFIG=${FUNC}_CONFIG # <== variable like "metis_CONFIG" or similar
    SELECTED_CONFIG=${SELECTED_CONFIG^^}
    eval CUSTOM_CONFIG=\$${SELECTED_CONFIG}
    if [ -n "$CUSTOM_CONFIG" ]; then
        FINAL_CONFIG="_$CUSTOM_CONFIG"
        # test func
        FINAL_FUNC="${OPCODE}_${FUNC}${FINAL_CONFIG}"
        if [ -n "$(type -t ${FINAL_FUNC})" ] && [ "$(type -t ${FINAL_FUNC})" = function ]; then
            # yes, call it
            ${FINAL_FUNC}
            return
        else
            # error
            echo "${FINAL_FUNC} is a customization but is not found!"
            exit 1
        fi
    fi
    # try with machine config
    if [ "$MACHINE_CONFIG" != "NOCONFIG" ]; then
        FINAL_CONFIG="_$MACHINE_CONFIG"
        # test func
        FINAL_FUNC="${OPCODE}_${FUNC}${FINAL_CONFIG}"
        if [ -n "$(type -t ${FINAL_FUNC})" ] && [ "$(type -t ${FINAL_FUNC})" = function ]; then
            # yes, call it
            ${FINAL_FUNC}
            return
        fi
    fi
    # use standard one, without config
    ${OPCODE}_${FUNC}
}

# escape the replace strings for use with sed
function escapeForSed {
    RET=$(echo $@ | sed -e 's/[]\/$*.^|[]/\\&/g')
    echo $RET
}

# prepare sed script
function prepareSedScript {
export SED_TMP=/tmp/sed.libs.$$
{
echo "s/\[LIBRARIES_BASE\]/$(escapeForSed $LIBRARIES_BASE)/g"
echo "s/\[LIBRARIES_PREFIX\]/$(escapeForSed $LIBRARIES_PREFIX)/g"
echo "s/\[LIBRARIES_PREFIX_DEBUG\]/$(escapeForSed $LIBRARIES_PREFIX_DEBUG)/g"
echo "s/\[C_COMPILER\]/$(escapeForSed $C_COMPILER)/g"
echo "s/\[CXX_COMPILER\]/$(escapeForSed $CXX_COMPILER)/g"
echo "s/\[FORTRAN_COMPILER\]/$(escapeForSed $FORTRAN_COMPILER)/g"
echo "s/\[MPI_C_COMPILER\]/$(escapeForSed $MPI_C_COMPILER)/g"
echo "s/\[MPI_CXX_COMPILER\]/$(escapeForSed $MPI_CXX_COMPILER)/g"
echo "s/\[MPI_FORTRAN_COMPILER\]/$(escapeForSed $MPI_FORTRAN_COMPILER)/g"
echo "s/\[BUILD_TYPE\]/$(escapeForSed $BUILD_TYPE)/g"
echo "s/\[\]/$(escapeForSed $)/g"
} > $SED_TMP
trap "rm -f $SED_TMP; exit 1" 0 1 2 3 13 15  # aka EXIT HUP INT QUIT PIPE TERM
}

# function to apply sed script to a file
function applySedScript {
    # $1 = filename
    #FILE_PATH=$(dirname "$1")
    #FILE_NAME=$(basename "$1")
    #FILE_EXT=$([[ "$FILE_NAME" = *.* ]] && echo "${FILE_NAME##*.}" || echo '')
    #echo $1
    sed -f $SED_TMP "$1" > "/tmp/sed.out"
    mv "/tmp/sed.out" "$1"
}

# INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
function getInstallDir {
    if [ "$SEPARATE_INSTALL" != "1" ]; then
        RET="${LIBRARIES_PREFIX}"
    else
        BASE_NAME="$1"
        VERSION="$2"
        if [ "$VERSION" != "" ]; then
            BASE_NAME_2=${BASE_NAME}-${VERSION}
        else
            BASE_NAME_2=${BASE_NAME}
        fi
        RET="${LIBRARIES_BASE}/${BASE_NAME_2}_Install${COMPILER_SUFFIX}"
    fi
    echo $RET
}

# BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
function getBuildDir {
    BASE_NAME="$1"
    VERSION="$2"
    if [ "$VERSION" != "" ]; then
        BASE_NAME_2=${BASE_NAME}-${VERSION}
    else
        BASE_NAME_2=${BASE_NAME}
    fi
    RET="${LIBRARIES_BASE}/${BASE_NAME_2}_Build${COMPILER_SUFFIX}"
    echo $RET
}

# BUILD_DIR=$( getBuildDir2 ${BASE_NAME} ${VERSION} )
function getBuildDir2 {
    BASE_NAME="$1"
    VERSION="$2"
    if [ "$VERSION" != "" ]; then
        BASE_NAME_2=${BASE_NAME}-${VERSION}
    else
        BASE_NAME_2=${BASE_NAME}
    fi
    RET="${LIBRARIES_BASE}/${BASE_NAME_2}_Build${COMPILER_SUFFIX}${BUILD_TYPE}"
    echo $RET
}


#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Example of configuration script.
# Copy this to "libsetup_config.sh" and customize it.
#
# Version: 1.0
#
# 07/10/2015 - First version
#================================================================================#

# define this to use Intel compiler
#export USE_COMPILER="intel"

source libsetup_config_base.sh

# create a common install prefix or separated
export SEPARATE_INSTALL=1

export LIBRARIES_BASE=/usr/scratch/bartezza/Libraries
#export LIBRARIES_BASE=/usr/scratch/bartezza/Libraries2

export LIBRARIES_PREFIX=$LIBRARIES_BASE/Libraries
export LIBRARIES_PREFIX_DEBUG=$LIBRARIES_BASE/LibrariesDebug

#export C_COMPILER="gcc"
#export CXX_COMPILER="g++"

#export MPI_C_COMPILER="mpicc"
#export MPI_CXX_COMPILER="mpic++"  # on MacOS: "mpic++-mpich-mp"

# machine config
# builtin configs: superb
#export MACHINE_CONFIG="superb"

# example of custom config
#export metis_CONFIG=example

# library versions
#export METIS_VERSION=5.1.0

#export PARMETIS_VERSION=4.0.3

#export TRILINOS_ISOGLIB_VERSION=12.2.1

#export CMAKE_VERSION=3.4.0-rc1
#export CMAKE_VERSION_MAJOR=3.4

#export CURL_VERSION=7.45.0

#export OPENSSL_VERSION=1.0.2d

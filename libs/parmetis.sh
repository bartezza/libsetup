#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Parmetis library installer
# Webpage: http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download
#
# Version: 1.0
#
# 07/10/2015 - First version
#================================================================================#

function enable_parmetis {
    # default enable
    libraries_enable "parmetis" ${PARMETIS_VERSION}
    # export things
    export PARMETIS_BASE_DIR=${CUR_BASE_DIR}
    export PARMETIS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export PARMETIS_LIB_DIR=${CUR_LIB_DIR}
    #export PARMETIS_LIBRARIES=${CUR_LIBRARIES}
}

function enable_parmetis_superb {
    if [ "$USE_COMPILER" = "gcc" ]; then
        source "/software/ENV/set_parmetis-gnu-base.sh"
        export PARMETIS_BASE=$INSTDIR
        export PARMETIS_INCLUDE_DIR=$INSTDIR/include
        export PARMETIS_LIB_DIR=$INSTDIR/lib
        echo "Using SuperB parmetis"
    else
        echo "[Error] Unsupported compiler '${USE_COMPILER}'"
        exit 1
    fi
}

function enable_parmetis_bellatrixNO {
    module load parmetis
    export PARMETIS_BASE=$PARMETIS_ROOT
    #export PARMETIS_INCLUDE=$PARMETIS_INCLUDE
    export PARMETIS_LIB_DIR=$PARMETIS_LIBRARY
    echo "Using module for parmetis"
}

function install_parmetis {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    # version
    BASE_NAME=parmetis
    VERSION=$PARMETIS_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    ${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
          -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
          -DCMAKE_C_COMPILER:STRING=$MPI_C_COMPILER \
          -DCMAKE_CXX_COMPILER:STRING=$MPI_CXX_COMPILER \
          -DGKLIB_PATH=`pwd`/../$BASE_DIR/metis/GKlib \
          -DMETIS_PATH=`pwd`/../$BASE_DIR/metis \
          -DOPENMP=ON \
          ../$BASE_DIR
    make -j8 install
    #cd metis
    #export PATH=${LIBRARIES_PREFIX}/bin:${PATH} # for cmake
    #make config prefix=${LIBRARIES_PREFIX}
    #make -j8 install
    #cd ..
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}

function uninstall_parmetis {
    libraries_uninstallLib "parmetis" $PARMETIS_VERSION
}

function clean_parmetis {
    libraries_cleanLib "parmetis" $PARMETIS_VERSION
}


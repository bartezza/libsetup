#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Tidy library installer
# Webpage: http://sourceforge.net/p/tidy/code/?source=navbar
#
# Version: 1.0
#
# 26/10/2015 - First version
#================================================================================#

function enable_tidy {
    # default dirs
    TIDY_INCLUDE_DIR=
    TIDY_LIB_DIR=
    # check if already installed
    BASE_NAME=tidy
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        TIDY_INCLUDE_DIR=$LIBRARIES_PREFIX/include
        TIDY_LIB_DIR=$LIBRARIES_PREFIX/lib
        echo "Using custom ${BASE_NAME}"
    else
        echo "Using system installed ${BASE_NAME}"
    fi
}

#function enable_tidy_superb {
#    source "/software/ENV/set_tidy-gnu-base.sh"
#    export TIDY_BASE=$INSTDIR
#    export TIDY_INCLUDE_DIR=$INSTDIR/include
#    export TIDY_LIB_DIR=$INSTDIR/lib
#    echo "Using SuperB tidy"
#}

function install_tidy {
    # version
    BASE_NAME=tidy
    VERSION=$TIDY_VERSION
    # check if already installed
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # clone repo
    BASE_DIR=${BASE_NAME}
    if [ ! -d "$BASE_DIR" ]; then
        cvs -d:pserver:anonymous@tidy.cvs.sourceforge.net:/cvsroot/tidy login
        cvs -z3 -d:pserver:anonymous@tidy.cvs.sourceforge.net:/cvsroot/tidy co -P tidy
    else
        # TODO: update from repo!
        echo TODO: update from repo!
    fi
    # autotools
    cd $BASE_DIR
    bash build/gnuauto/setup.sh
    cd ..
    # configure
    BUILD_DIR=${BASE_DIR}_Build
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    ../$BASE_DIR/configure --prefix=$LIBRARIES_PREFIX
    make -j8
    make install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


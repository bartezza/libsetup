#!/bin/bash

function enable_hdf5 {
    # default dirs
    HDF5_INCLUDE_DIR=
    HDF5_LIB_DIR=
    # check if already installed
    BASE_NAME=hdf5
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        HDF5_INCLUDE_DIR=$LIBRARIES_PREFIX/include
        HDF5_LIB_DIR=$LIBRARIES_PREFIX/lib
        echo "Using custom ${BASE_NAME}"
    else
        echo "Using system installed ${BASE_NAME}"
    fi
}

function enable_hdf5_superb {
    source "/software/ENV/set_hdf5-parallel-gnu-base.sh"
    export HDF5_INCLUDE_DIR=$INSTDIR/include
    export HDF5_LIB_DIR=$INSTDIR/lib
    echo "Using SuperB HDF5"
}

function enable_hdf5_bellatrixNO {
    #module load hdf5/1.8.14/gcc-4.4.7
    module load hdf5/1.8.14/intel-15.0.0
    export HDF5_INCLUDE_DIR=$HDF5_INCLUDE
    export HDF5_LIB_DIR=$HDF5_LIBRARY
    echo "Using module for hdf5"
}

function install_hdf5 {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    # version
    BASE_NAME=hdf5
    VERSION=$HDF5_VERSION
    # check if already installed
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.bz2
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://www.hdfgroup.org/ftp/HDF5/current/src/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    # configure
    BUILD_DIR=${BASE_DIR}_Build
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    ${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
          -DCMAKE_INSTALL_PREFIX=$LIBRARIES_PREFIX \
          -DCMAKE_C_COMPILER:STRING=$MPI_C_COMPILER \
          -DCMAKE_CXX_COMPILER:STRING=$MPI_CXX_COMPILER \
          -DHDF5_ENABLE_PARALLEL=ON \
          -DHDF5_BUILD_CPP_LIB=OFF \
          ../$BASE_DIR
    make -j8 install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


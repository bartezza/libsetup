#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Trilinos library installer - Customized for being used with isoGlib
#
# Version: 1.0
#
# 07/10/2015 - First version
#================================================================================#

function enableRequired_trilinos_isoglib {
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    callFunc enable metis
    callFunc enable parmetis
    callFunc enable boost
    #callFunc enable hdf5
    callFunc enable umfpack
    callFunc enable amd
    callFunc enable blas
    callFunc enable lapack
    callFunc enable scalapack
    if [ "$TRILINOS_USE_MUMPS" = "1" ]; then
        callFunc enable blacs
        callFunc enable scotch
        callFunc enable mumps
    fi
}

function enable_trilinos_isoglib {
    # required libraries
    enableRequired_trilinos_isoglib
    # default enable
    libraries_enable "trilinos_isoglib" ${TRILINOS_ISOGLIB_VERSION}
    # export things
    export TRILINOS_BASE_DIR=${CUR_BASE_DIR}
    export TRILINOS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export TRILINOS_LIB_DIR=${CUR_LIB_DIR}
    echo "[Trilinos] baseDir = ${TRILINOS_BASE_DIR}, includeDir = ${TRILINOS_INCLUDE_DIR}, libDir = ${TRILINOS_LIB_DIR}"
}

function install_trilinos_isoglib {
    # required libraries
    enableRequired_trilinos_isoglib
    # version
    BASE_NAME=trilinos_isoglib
    BASE_NAME_2=trilinos
    VERSION=$TRILINOS_ISOGLIB_VERSION
    # prompt
    P="[${Cyan}${BASE_NAME}${Color_Off}]"
    
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" = "install" ]; then
        if [ -f "$INSTALLED" ]; then
            echo -e "$P Already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}-Source
    BASE_DIR_2=${BASE_NAME_2}-${VERSION}-Source
    PACKAGE=${BASE_DIR_2}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            echo -e "$P Downloading $PACKAGE..."
            wget "http://trilinos.csbsju.edu/download/files/${PACKAGE}"
        else
            echo -e "$P Already downloaded"
        fi
        rm -rf ${BASE_DIR_2}
        echo -e "$P Extracting..."
        tar -xf $PACKAGE
        # change dir name
        mv ${BASE_DIR_2} ${BASE_DIR}
    else
        echo -e "$P Sources already extracted"
    fi
    
    # configure
    BUILD_DIR=$( getBuildDir2 ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    if [ "$DO_NOT_CONFIGURE" != "1" ]; then
        echo -e "$P Configuring..."
        OTHER_TPLS=""
        # MUMPS?
        if [ "$TRILINOS_USE_MUMPS" = "1" ]; then
            OTHER_TPLS+="-D Amesos_ENABLE_BLACS:BOOL=ON "
            OTHER_TPLS+="-D TPL_ENABLE_BLACS:BOOL=ON \
              `cmakeDefine TPL_BLACS_INCLUDE_DIRS:PATH ${BLACS_INCLUDE_DIR}` \
              `cmakeDefine BLACS_LIBRARY_DIRS:PATH ${BLACS_LIB_DIR}` \
              `cmakeDefine TPL_BLACS_LIBRARIES:PATH ${BLACS_LIBRARIES}` "
            OTHER_TPLS+="-D TPL_ENABLE_Scotch:BOOL=ON \
                `cmakeDefine Scotch_INCLUDE_DIRS:PATH ${SCOTCH_INCLUDE_DIR}` \
                `cmakeDefine Scotch_LIBRARY_DIRS:PATH ${SCOTCH_LIB_DIR}` "
            OTHER_TPLS+="-D TPL_ENABLE_MUMPS:BOOL=ON \
                `cmakeDefine MUMPS_INCLUDE_DIRS:PATH ${MUMPS_INCLUDE_DIR}` \
                `cmakeDefine MUMPS_LIBRARY_DIRS:PATH ${MUMPS_LIB_DIR}` "
        else
            OTHER_TPLS+="-D TPL_ENABLE_BLACS:BOOL=OFF "
            OTHER_TPLS+="-D TPL_ENABLE_Scotch:BOOL=OFF "
            OTHER_TPLS+="-D TPL_ENABLE_MUMPS:BOOL=OFF "
        fi
        # cmd
        CMD="${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
              -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
              -DCMAKE_C_COMPILER:STRING=$MPI_C_COMPILER \
              -DCMAKE_CXX_COMPILER:STRING=$MPI_CXX_COMPILER \
              -DCMAKE_Fortran_COMPILER:STRING=$MPI_FORTRAN_COMPILER \
              -DMPI_C_COMPILER:STRING=$MPI_C_COMPILER \
              -DMPI_CXX_COMPILER:STRING=$MPI_CXX_COMPILER \
              -DMPI_Fortran_COMPILER:STRING=$MPI_FORTRAN_COMPILER \
              -DMPI_EXEC:STRING=$MPI_EXEC \
              -D TPL_ENABLE_MPI:BOOL=ON \
              -D TPL_ENABLE_BLAS:BOOL=ON \
              -D TPL_ENABLE_LAPACK:BOOL=ON \
              -D TPL_ENABLE_HDF5:BOOL=OFF \
              -D TPL_ENABLE_ParMETIS:BOOL=ON \
              -D TPL_ENABLE_METIS:BOOL=ON \
              -D TPL_ENABLE_Boost:BOOL=OFF \
              -D TPL_ENABLE_Zlib:BOOL=ON \
              -D TPL_ENABLE_Pthread:BOOL=ON \
              -D TPL_ENABLE_UMFPACK:BOOL=ON \
              -D TPL_ENABLE_AMD:BOOL=ON \
              ${OTHER_TPLS} \
              -D Trilinos_ENABLE_Teuchos:BOOL=ON \
              -D Trilinos_ENABLE_Epetra:BOOL=ON \
              -D Trilinos_ENABLE_EpetraExt:BOOL=ON \
              -D Trilinos_ENABLE_AztecOO:BOOL=ON \
              -D Trilinos_ENABLE_Amesos:BOOL=ON \
              -D Trilinos_ENABLE_Ifpack:BOOL=ON \
              -D Trilinos_ENABLE_Belos:BOOL=ON \
              -D Trilinos_ENABLE_Tpetra:BOOL=OFF \
              -D Trilinos_ENABLE_Xpetra:BOOL=OFF \
              -D Amesos_ENABLE_MUMPS:BOOL=ON \
              -D Amesos_ENABLE_SCALAPACK:BOOL=ON \
              `cmakeDefine Boost_INCLUDE_DIRS:PATH ${BOOST_INCLUDE_DIR}` \
              `cmakeDefine UMFPACK_INCLUDE_DIRS:PATH ${UMFPACK_INCLUDE_DIR}` \
              `cmakeDefine UMFPACK_LIBRARY_DIRS:PATH ${UMFPACK_LIB_DIR}` \
              `cmakeDefine AMD_INCLUDE_DIRS:PATH ${AMD_INCLUDE_DIR}` \
              `cmakeDefine AMD_LIBRARY_DIRS:PATH ${AMD_LIB_DIR}` \
              `cmakeDefine HDF5_INCLUDE_DIRS:PATH ${HDF5_INCLUDE_DIR}` \
              `cmakeDefine ParMETIS_INCLUDE_DIRS:PATH ${PARMETIS_INCLUDE_DIR}` \
              `cmakeDefine ParMETIS_LIBRARY_DIRS:PATH ${PARMETIS_LIB_DIR}` \
              `cmakeDefine METIS_INCLUDE_DIRS:PATH ${METIS_INCLUDE_DIR}` \
              `cmakeDefine METIS_LIBRARY_DIRS:PATH ${METIS_LIB_DIR}` \
              `cmakeDefine BLAS_LIBRARY_DIRS:PATH ${BLAS_LIB_DIR}` \
              `cmakeDefine TPL_BLAS_LIBRARIES:PATH ${BLAS_LIBRARIES}` \
              `cmakeDefine LAPACK_LIBRARY_DIRS:PATH ${LAPACK_LIB_DIR}` \
              `cmakeDefine TPL_LAPACK_LIBRARIES:PATH ${LAPACK_LIBRARIES}` \
              `cmakeDefine TPL_BLACS_LIBRARIES:PATH ${BLACS_LIBRARIES}` \
              `cmakeDefine SCALAPACK_LIBRARY_DIRS:PATH ${SCALAPACK_LIB_DIR}` \
              `cmakeDefine TPL_SCALAPACK_LIBRARIES:PATH ${SCALAPACK_LIBRARIES}` \
              ../$BASE_DIR"
        if [ "$DO_ECHO_CONFIGURE_ONLY" = "1" ]; then
            echo $CMD
            exit 1
        fi
        $CMD
        pause
    fi
    echo -e "$P Building..."
    make -j${MAKE_JOBS}
    
    # install
    if [ "$DO_INSTALL" = "1" ]; then
        # install
        echo -e "$P Installing..."
        make install
        # set as installed
        echo $VERSION > $INSTALLED
        echo -e "$P Version $VERSION installed"
    fi
    
    cd ..
}


#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Scotch library installer
# Webpage: http://gforge.inria.fr/projects/scotch/
#
# Version: 1.0
#
# 08/06/2016 - First version
#================================================================================#

function enable_scotch {
    # default enable
    libraries_enable "scotch" ${SCOTCH_VERSION}
    # export things
    export SCOTCH_BASE_DIR=${CUR_BASE_DIR}
    export SCOTCH_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export SCOTCH_LIB_DIR=${CUR_LIB_DIR}
    #export SCOTCH_LIBRARIES=${CUR_LIBRARIES}
}

#function enable_scotch_superb {
#    source "/software/ENV/set_scotch-gnu-base.sh"
#    export SCOTCH_BASE=$INSTDIR
#    export SCOTCH_INCLUDE_DIR=$INSTDIR/include
#    export SCOTCH_LIB_DIR=$INSTDIR/lib
#    echo "Using SuperB scotch"
#}

#function enable_scotch_bellatrixNO {
#    module load scotch
#    export SCOTCH_BASE=$SCOTCH_ROOT
#    #export SCOTCH_INCLUDE=$SCOTCH_INCLUDE
#    export SCOTCH_LIB_DIR=$SCOTCH_LIBRARY
#    echo "Using module for scotch"
#}

function install_scotch {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    # version
    BASE_NAME=scotch
    VERSION=$SCOTCH_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}_${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://gforge.inria.fr/frs/download.php/file/34618/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    
    # copy srcs to builddir
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cp -r ${BASE_DIR}/* ${BUILD_DIR}/
    
    # copy makefile
    cp ${LIBSETUP_BASE}/libs/${BASE_NAME}_${VERSION}_Makefile.inc.x86-64_pc_linux2 ${BUILD_DIR}/src/Makefile.inc
    # apply template
    prepareSedScript
    applySedScript "${BUILD_DIR}/src/Makefile.inc"
    
    # make
    cd ${BUILD_DIR}/src
    make -j${MAKE_JOBS} prefix=${INSTALL_DIR} scotch
    make -j${MAKE_JOBS} prefix=${INSTALL_DIR} ptscotch
    make -j${MAKE_JOBS} prefix=${INSTALL_DIR} esmumps
    
    # install
    mkdir -p ${INSTALL_DIR}
    mkdir -p ${INSTALL_DIR}/include
    mkdir -p ${INSTALL_DIR}/lib
    mkdir -p ${INSTALL_DIR}/bin
    make prefix=${INSTALL_DIR} install
    cp -v ../lib/lib*.a ${INSTALL_DIR}/lib/
    cd ../..
    
    # set as installed
    echo $VERSION > $INSTALLED
}

function uninstall_scotch {
    libraries_uninstallLib "scotch" $SCOTCH_VERSION
}

function clean_scotch {
    libraries_cleanLib "scotch" $SCOTCH_VERSION
}


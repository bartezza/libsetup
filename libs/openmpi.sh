#!/bin/bash
#
# TODO: rename this to mpi

function enable_openmpi {
    #export MPI_C_COMPILER="mpicc"
    #export MPI_CXX_COMPILER="mpic++"
    #export MPI_FORTRAN_COMPILER="mpifort"
    #export MPI_INCLUDE_DIR="/usr/include/openmpi"
    #export MPI_LIB_DIR=
    #export MPI_LIBRARIES=
    echo "Using system installed MPI"
}

function enable_openmpi_superb {
    if [ "$USE_COMPILER_MPI" = "gcc-openmpi" ]; then
        source "/software/ENV/set_openmpi-gnu-base.sh"
        export MPI_BASE=$INSTDIR
        echo "Using SuperB openmpi"
    else
        echo "Compiler '$USE_COMPILER_MPI' not supported for this configuration"
        exit 1
    fi
}

function enable_openmpi_bellatrix {
    if [ "$USE_COMPILER_MPI" = "gcc-openmpi" ]; then
        export MPI_BASE=/ssoft/openmpi/1.6.5
        export MPI_C_COMPILER=$MPI_BASE/bin/mpicc
        export MPI_CXX_COMPILER=$MPI_BASE/bin/mpic++
        export LD_LIBRARY_PATH=$MPI_BASE/lib:$LD_LIBRARY_PATH
        echo "Using /ssoft openmpi"
    elif [ "$USE_COMPILER_MPI" = "intel-mpi" ]; then
        module load intelmpi
        export MPI_C_COMPILER="${I_MPI_ROOT}/bin64/mpicc"
        export MPI_CXX_COMPILER="${I_MPI_ROOT}/bin64/mpicxx"
        if [ "0" = "1" ]; then
            export I_MPI_CC="icc"
            export I_MPI_CXX="icc"
        fi
        echo "Using module for intel mpi"
    else
        echo "Compiler '$USE_COMPILER_MPI' not supported for this configuration"
        exit 1
    fi
}


#!/bin/bash

function install_trilinos {
    # required libraries
    enable_gcc
    enable_openmpi
    enable_cmake
}


#!/bin/bash

function enable_cmake {
    # default command
    CMAKE_BIN=cmake
    # check if already installed
    BASE_NAME=cmake
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        CMAKE_BIN=$LIBRARIES_PREFIX/bin/cmake
        echo "Using custom ${BASE_NAME}"
    else
        echo "Using system installed ${BASE_NAME}"
    fi
}

function enable_cmake_bellatrixNO {
    # load module
    module load cmake
    CMAKE_BIN=cmake
    echo "Using module for cmake" 
}

function install_cmake {
    # version
    BASE_NAME=cmake
    VERSION=$CMAKE_VERSION
    # prompt
    P="[${Cyan}${BASE_NAME}${Color_Off}]"
    # check if already installed
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo -e "$P Already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            echo -e "$P Downloading $PACKAGE..."
            wget --no-check-certificate "http://www.cmake.org/files/v${CMAKE_VERSION_MAJOR}/${PACKAGE}"
        else
            echo -e "$P Already downloaded"
        fi
        echo -e "$P Extracting..."
        tar -xf $PACKAGE
    else
        echo -e "$P Sources already extracted"
    fi
    # configure
    BUILD_DIR=${BASE_DIR}_Build
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    #echo -e "$P Configuring..."
    #cmake -DCMAKE_BUILD_TYPE=Release \
    #      -DCMAKE_INSTALL_PREFIX=$LIBRARIES_PREFIX \
    #      -DCMAKE_C_COMPILER:STRING=$C_COMPILER \
    #      -DCMAKE_CXX_COMPILER:STRING=$CXX_COMPILER \
    #      ../$BASE_DIR
    echo -e "$P Bootstrapping..."
    ../$BASE_DIR/bootstrap --prefix=$LIBRARIES_PREFIX
    echo -e "$P Building..."
    make -j8 install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
    echo -e "$P Version $VERSION installed"
}

function uninstall_cmake {
    libraries_uninstallLib "cmake" $CMAKE_VERSION
}

function clean_cmake {
    libraries_cleanLib "cmake" $CMAKE_VERSION
}


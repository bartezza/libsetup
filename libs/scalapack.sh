#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015-2016
#
# SCALAPACK library installer
# Webpage: http://www.netlib.org/scalapack/
#
# Version: 1.0
#
# 08/06/2016 - First version
#================================================================================#

function enable_scalapack {
    if [ "$USE_COMPILER" = "intel" ]; then
        enable_scalapack_mkl
    else
        enable_scalapack_default
    fi
}

function enable_scalapack_mkl {
    # enable blacs
    #callFunc enable blacs
    # dirs
    export SCALAPACK_INCLUDE_DIR="$INTEL_MKL_BASE/include"
    export SCALAPACK_LIB_DIR="$INTEL_MKL_BASE/lib/intel64"
    export SCALAPACK_LIBRARIES="${SCALAPACK_LIB_DIR}/libmkl_scalapack_lp64.a"
    echo "Using Intel MKL for scalapack"
}

function enable_scalapack_default {
    # enable blacs
    #callFunc enable blacs
    # default enable
    libraries_enable "scalapack" ${SCALAPACK_VERSION}
    # export things
    export SCALAPACK_BASE_DIR=${CUR_BASE_DIR}
    export SCALAPACK_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export SCALAPACK_LIB_DIR=${CUR_LIB_DIR}
    export SCALAPACK_LIBRARIES="${CUR_LIB_DIR}/libscalapack.a"
}

#function enable_scalapack_superb {
#    source "/software/ENV/set_scalapack-gnu-base.sh"
#    export SCALAPACK_BASE=$INSTDIR
#    export SCALAPACK_INCLUDE_DIR=$INSTDIR/include
#    export SCALAPACK_LIB_DIR=$INSTDIR/lib
#    echo "Using SuperB scalapack"
#}

#function enable_scalapack_bellatrixNO {
#    module load scalapack
#    export SCALAPACK_BASE=$SCALAPACK_ROOT
#    #export SCALAPACK_INCLUDE=$SCALAPACK_INCLUDE
#    export SCALAPACK_LIB_DIR=$SCALAPACK_LIBRARY
#    echo "Using module for scalapack"
#}

function install_scalapack {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    #callFunc enable blas # enabled by lapack
    callFunc enable lapack
    # version
    BASE_NAME=scalapack
    VERSION=$SCALAPACK_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tgz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://www.netlib.org/scalapack/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    # NOTE: forcing FORTRAN_COMPILER instead of MPI_FORTRAN_COMPILER otherwise it doesn't work!
    CMD="${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
          -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
          -DCMAKE_C_COMPILER:STRING=$MPI_C_COMPILER \
          -DCMAKE_CXX_COMPILER:STRING=$MPI_CXX_COMPILER \
          -DCMAKE_Fortran_COMPILER:STRING=$MPI_FORTRAN_COMPILER \
          -DBUILD_TESTING=NO \
          -DLAPACK_LIBRARIES=$LAPACK_LIBRARIES \
          -DBLAS_LIBRARIES=$BLAS_LIBRARIES \
          ../$BASE_DIR"
    echo $CMD
    $CMD
    pause
    make -j${MAKE_JOBS}
    # install
    make install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}

function uninstall_scalapack {
    libraries_uninstallLib "scalapack" $SCALAPACK_VERSION
}

function clean_scalapack {
    libraries_cleanLib "scalapack" $SCALAPACK_VERSION
}


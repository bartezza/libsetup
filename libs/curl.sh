#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Curl library installer
# Webpage: http://curl.haxx.se/download.html
#
# Version: 1.0
#
# 26/10/2015 - First version
#================================================================================#

function enable_curl {
    # default dirs
    CURL_INCLUDE_DIR=
    CURL_LIB_DIR=
    # check if already installed
    BASE_NAME=curl
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        CURL_INCLUDE_DIR=$LIBRARIES_PREFIX/include
        CURL_LIB_DIR=$LIBRARIES_PREFIX/lib
        echo "Using custom ${BASE_NAME}"
    else
        echo "Using system installed ${BASE_NAME}"
    fi
}

#function enable_curl_superb {
#    source "/software/ENV/set_curl-gnu-base.sh"
#    export CURL_BASE=$INSTDIR
#    export CURL_INCLUDE_DIR=$INSTDIR/include
#    export CURL_LIB_DIR=$INSTDIR/lib
#    echo "Using SuperB curl"
#}

function install_curl {
    callFunc enable cmake
    callFunc enable openssl
    # version
    BASE_NAME=curl
    VERSION=$CURL_VERSION
    # check if already installed
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://curl.haxx.se/download/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    # configure
    BUILD_DIR=${BASE_DIR}_Build
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    ${CMAKE_BIN} -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_INSTALL_PREFIX=$LIBRARIES_PREFIX \
          -DCMAKE_C_COMPILER:STRING=$C_COMPILER \
          -DCMAKE_CXX_COMPILER:STRING=$CXX_COMPILER \
          ../$BASE_DIR
    make -j8 install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


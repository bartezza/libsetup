#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2017
#
# LUA library installer
# Webpage: https://www.lua.org/download.html
#
# Version: 1.0
#
# 05/03/2017 - First version
#================================================================================#

function enable_lua {
    # default enable
    libraries_enable "lua" ${LUA_VERSION}
    # export things
    export LUA_BASE_DIR=${CUR_BASE_DIR}
    export LUA_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export LUA_LIB_DIR=${CUR_LIB_DIR}
    export LUA_LIBRARIES=${LUA_LIB_DIR}/liblua.a
}

function install_lua {
    #callFunc enable cmake
    callFunc enable gcc
    # version
    BASE_NAME=lua
    VERSION=$LUA_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget -R -O http://www.lua.org/ftp/${PACKAGE}
        fi
        tar -xf $PACKAGE
    fi
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    #mkdir -p $BUILD_DIR
    #BUILD_DIR=${BASE_DIR}
    cp -r $BASE_DIR $BUILD_DIR
    cd $BUILD_DIR
    
    # copy makefile
    #cp ${LIBSETUP_BASE}/libs/${BASE_NAME}_${VERSION}_Makefile ${BUILD_DIR}/src/Makefile.inc
    # apply template
    #prepareSedScript
    #applySedScript "${BUILD_DIR}/src/Makefile.inc"
    
    # make
    #make -j8 linux
    MYCFLAGS=-fPIC make -e -j8 linux
    cd ..
    
    # install
    mkdir -p ${INSTALL_DIR}
    mkdir -p ${INSTALL_DIR}/include
    mkdir -p ${INSTALL_DIR}/lib
    mkdir -p ${INSTALL_DIR}/bin
    cp -v ${BUILD_DIR}/src/*.a ${INSTALL_DIR}/lib/
    cp -v ${BUILD_DIR}/src/lua.h ${INSTALL_DIR}/include/
    cp -v ${BUILD_DIR}/src/lua.hpp ${INSTALL_DIR}/include/
    cp -v ${BUILD_DIR}/src/luaconf.h ${INSTALL_DIR}/include/
    cp -v ${BUILD_DIR}/src/lualib.h ${INSTALL_DIR}/include/
    cp -v ${BUILD_DIR}/src/lauxlib.h ${INSTALL_DIR}/include/
    cd ../..
    
    # set as installed
    echo $VERSION > $INSTALLED
}


#!/bin/bash

function enable_suitesparse {
    # default enable
    libraries_enable "SuiteSparse" ${SUITESPARSE_VERSION}
    # export things
    export SUITESPARSE_BASE_DIR=${CUR_BASE_DIR}
    export SUITESPARSE_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export SUITESPARSE_LIB_DIR=${CUR_LIB_DIR}
}

function enable_suitesparse_superb {
    if [ "$USE_COMPILER" = "gcc" ]; then
        source "/software/ENV/set_suitesparse-gnu-base.sh"
        export SUITESPARSE_BASE_DIR=$INSTDIR
        export SUITESPARSE_INCLUDE_DIR=$INSTDIR/include
        export SUITESPARSE_LIB_DIR=$INSTDIR/lib
        echo "Using SuperB suitesparse"
    else
        echo "[Error] Unsupported compiler '${USE_COMPILER}'"
        exit 1  
    fi
}

function install_suitesparse {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    callFunc enable metis
    callFunc enable blas
    # version
    BASE_NAME=SuiteSparse
    VERSION=$SUITESPARSE_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://faculty.cse.tamu.edu/davis/SuiteSparse/${PACKAGE}"
        fi
        tar -xf $PACKAGE
        mv ${BASE_NAME} ${BASE_DIR}
    fi
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    # copy from source dir to build dir
    if [ ! -d "$BUILD_DIR" ]; then
        mkdir -p $BUILD_DIR
        cp -r ${BASE_DIR}/* ${BUILD_DIR}/
    fi
    # enter build dir
    cd $BUILD_DIR
    # check compiler
    if [ "$USE_COMPILER" = "intel" ]; then
        # check if config files differs
        set +e
        DIFF=$( diff ${LIBSETUP_BASE}/libs/SuiteSparse_config_intel.mk ${BUILD_DIR}/SuiteSparse_config/SuiteSparse_config.mk )
        set -e
        if [ "$DIFF" != "" ]; then
            # copy config file
            cp -v ${LIBSETUP_BASE}/libs/SuiteSparse_config_intel.mk ${BUILD_DIR}/SuiteSparse_config/SuiteSparse_config.mk
        fi
    fi
    # make
    export CUDA=no
    export BLAS="$BLAS_LIBRARIES -lpthread"
    export LAPACK="$LAPACK_LIBRARIES -lpthread"
    export MY_METIS_INC=$METIS_INCLUDE_DIR
    export MY_METIS_LIB=$METIS_LIBRARIES
    make -j8 library #CUDA=no BLAS=${BLAS_LIBRARIES} LAPACK=${LAPACK_LIBRARIES}
    # install
    mkdir -p $INSTALL_DIR
    mkdir -p $INSTALL_DIR/include
    mkdir -p $INSTALL_DIR/lib
    export INSTALL_INCLUDE=${INSTALL_DIR}/include
    export INSTALL_LIB=${INSTALL_DIR}/lib
    make install #CUDA=no INSTALL_LIB=${INSTALL_DIR}/lib BLAS=${BLAS_LIBRARIES} LAPACK=${LAPACK_LIBRARIES}
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


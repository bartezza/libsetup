#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Metis library installer
# Webpage: http://glaros.dtc.umn.edu/gkhome/metis/metis/download
#
# Version: 1.0
#
# 07/10/2015 - First version
#================================================================================#

function enable_metis {
    # default enable
    libraries_enable "metis" ${METIS_VERSION}
    # export things
    export METIS_BASE_DIR=${CUR_BASE_DIR}
    export METIS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export METIS_LIB_DIR=${CUR_LIB_DIR}
    export METIS_LIBRARIES=${METIS_LIB_DIR}/libmetis.a
}

function install_metis {
    callFunc enable cmake
    callFunc enable gcc
    # version
    BASE_NAME=metis
    VERSION=$METIS_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    #BUILD_DIR=${BASE_DIR}
    cd $BUILD_DIR
    ${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
          -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
          -DCMAKE_C_COMPILER:STRING=$C_COMPILER \
          -DCMAKE_CXX_COMPILER:STRING=$CXX_COMPILER \
          -DGKLIB_PATH=`pwd`/../$BASE_DIR/GKlib \
          -DOPENMP=ON \
          ../$BASE_DIR
    make -j8 install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


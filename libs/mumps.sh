#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015-2016
#
# MUMPS library installer
#
# Version: 1.0
#
# 08/06/2016 - First version
#================================================================================#

function enableRequired_mumps {
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    callFunc enable parmetis
    #callFunc enable boost
    #callFunc enable hdf5
    #callFunc enable umfpack
    #callFunc enable amd
    callFunc enable blas
    #callFunc enable lapack
    callFunc enable blacs
    callFunc enable scalapack
    callFunc enable scotch
}

function enable_mumps {
    # required libraries
    enableRequired_mumps
    # mumps
    libraries_enable "mumps" ${MUMPS_VERSION}
    # export things
    export MUMPS_BASE_DIR=${CUR_BASE_DIR}
    export MUMPS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export MUMPS_LIB_DIR=${CUR_LIB_DIR}
    
    #MUMPS_BASE_DIR=/usr/local
    #MUMPS_INCLUDE_DIR=
    #MUMPS_LIB_DIR=
    ## check if already installed
    #BASE_NAME=mumps
    #INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    #if [ -f "$INSTALLED" ]; then
    #    # use custom one
    #    MUMPS_BASE_DIR=${LIBRARIES_PREFIX}
    #    MUMPS_INCLUDE_DIR=$LIBRARIES_PREFIX/include
    #    MUMPS_LIB_DIR=$LIBRARIES_PREFIX/lib
    #    echo "Using custom ${BASE_NAME}"
    #else
    #    echo "Using system installed ${BASE_NAME}"
    #fi
    ## export
    #export MUMPS_BASE_DIR
    #export MUMPS_INCLUDE_DIR
    #export MUMPS_LIB_DIR
}

function install_mumps {
    # required libraries
    enableRequired_mumps
    # version
    BASE_NAME=mumps
    BASE_NAME_2=MUMPS
    VERSION=$MUMPS_VERSION
    # prompt
    P="[${Cyan}${BASE_NAME}${Color_Off}]"
    
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" = "install" ]; then
        if [ -f "$INSTALLED" ]; then
            echo -e "$P Already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    BASE_DIR_2=${BASE_NAME_2}_${VERSION}
    PACKAGE=${BASE_DIR_2}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            echo -e "$P Downloading $PACKAGE..."
            wget "http://mumps.enseeiht.fr/${PACKAGE}"
        else
            echo -e "$P Already downloaded"
        fi
        rm -rf ${BASE_DIR_2}
        echo -e "$P Extracting..."
        tar -xf $PACKAGE
        # change dir name
        mv ${BASE_DIR_2} ${BASE_DIR}
    else
        echo -e "$P Sources already extracted"
    fi
    
     # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    
    # copy everything
    cp -r ${BASE_DIR}/* ${BUILD_DIR}/
    
    # copy makefile
    cp -v ${LIBSETUP_BASE}/libs/${BASE_NAME}_${VERSION}_Makefile.debian.PAR ${BUILD_DIR}/Makefile.inc
    # apply template
    prepareSedScript
    echo "s/\[LSCOTCHDIR\]/$(escapeForSed $SCOTCH_INCLUDE_DIR)/g" >> $SED_TMP
    echo "s/\[LMETISDIR\]/$(escapeForSed $PARMETIS_LIB_DIR)/g" >> $SED_TMP
    echo "s/\[IMETISDIR\]/$(escapeForSed $PARMETIS_INCLUDE_DIR)/g" >> $SED_TMP
    echo "s/\[IOPENMPI\]/$(escapeForSed $MPI_INCLUDE_DIR)/g" >> $SED_TMP
    echo "s/\[LBLAS\]/$(escapeForSed $BLAS_LIBRARIES)/g" >> $SED_TMP
    echo "s/\[LSCALAPACK\]/$(escapeForSed $SCALAPACK_LIBRARIES)/g" >> $SED_TMP
    echo "s/\[LBLACS\]/$(escapeForSed $BLACS_LIBRARIES)/g" >> $SED_TMP
    applySedScript "${BUILD_DIR}/Makefile.inc"
    
    # make
    cd ${BUILD_DIR}
    make -j${MAKE_JOBS} prefix=${INSTALL_DIR} alllib
    
    # manual install
    mkdir -p ${INSTALL_DIR}
    mkdir -p ${INSTALL_DIR}/include
    mkdir -p ${INSTALL_DIR}/lib
    cp -v include/* ${INSTALL_DIR}/include/
    cp -v lib/* ${INSTALL_DIR}/lib/
    
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Gperftools
# Webpage: https://github.com/gperftools/gperftools.git
#
# Version: 1.0
#
# 15 Nov 2016 - First version
#================================================================================#

function enable_gperftools {
    # default enable
    libraries_enable "gperftools" ${GPERFTOOLS_VERSION}
    # export things
    export GPERFTOOLS_BASE_DIR=${CUR_BASE_DIR}
    export GPERFTOOLS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export GPERFTOOLS_LIB_DIR=${CUR_LIB_DIR}
    export GPERFTOOLS_BIN_DIR=${CUR_BIN_DIR}
    #export GPERFTOOLS_LIBRARIES=${CUR_LIBRARIES}
}

function install_gperftools {
    # required libraries
    callFunc enable gcc
    #callFunc enable openmpi
    callFunc enable cmake
    # version
    BASE_NAME=gperftools
    VERSION="" #$GPERFTOOLS_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR="${BASE_NAME}" #-${VERSION}
    if [ ! -d "$BASE_DIR" ]; then
        git clone https://github.com/gperftools/gperftools.git ${BASE_DIR}
    else
        cd ${BASE_DIR}
        git pull
        cd ..
    fi
    # autoreconf
    cd ${BASE_DIR}
    ./autogen.sh
    cd ..
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    ../${BASE_DIR}/configure \
          --prefix=${INSTALL_DIR}
#          -DCMAKE_C_COMPILER:STRING=$MPI_C_COMPILER
#          -DCMAKE_CXX_COMPILER:STRING=$MPI_CXX_COMPILER
    make -j8 install
    #cd metis
    #export PATH=${LIBRARIES_PREFIX}/bin:${PATH} # for cmake
    #make config prefix=${LIBRARIES_PREFIX}
    #make -j8 install
    #cd ..
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}

function uninstall_gperftools {
    libraries_uninstallLib "gperftools" $GPERFTOOLS_VERSION
}

function clean_gperftools {
    libraries_cleanLib "gperftools" $GPERFTOOLS_VERSION
}


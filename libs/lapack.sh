#!/bin/bash

function enable_lapack {
    if [ "$LAPACK_TYPE" = "mkl" ]; then
        enable_lapack_mkl
    elif [ "$LAPACK_TYPE" = "openblas" ]; then
        enable_lapack_openblas
    elif [ "$LAPACK_TYPE" = "atlas" ]; then
        enable_lapack_atlas
    else
        enable_lapack_default
    fi
}

function enable_lapack_mkl {
    # lapack is inside MKL
    callFunc enable blas
    # dirs
    export LAPACK_INCLUDE_DIR="$INTEL_MKL_BASE/include"
    export LAPACK_LIB_DIR="$INTEL_MKL_BASE/lib/intel64"
    export LAPACK_LIBRARIES=${BLAS_LIBRARIES} # already set in BLAS libraries
    echo "Using Intel MKL for LAPACK"
}

function enable_lapack_openblas {
    callFunc enable blas
    # dirs
    export LAPACK_INCLUDE_DIR=${BLAS_INCLUDE_DIR}
    export LAPACK_LIB_DIR=${BLAS_LIB_DIR}
    export LAPACK_LIBRARIES=${BLAS_LIBRARIES}
    echo "Using openblas for LAPACK"
}

function enable_lapack_atlas {
    callfunc enable atlas
    # dirs
    export LAPACK_INCLUDE_DIR=${ATLAS_INCLUDE_DIR}
    export LAPACK_LIB_DIR=${ATLAS_LIB_DIR}
    export LAPACK_LIBRARIES="lapack"
    echo "Using atlas for LAPACK"
}

function enable_lapack_default {
    # default enable
    libraries_enable "lapack" ${LAPACK_VERSION}
    # export things
    export LAPACK_BASE_DIR=${CUR_BASE_DIR}
    export LAPACK_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export LAPACK_LIB_DIR=${CUR_LIB_DIR}
    export LAPACK_LIBRARIES="lapack"
}

function enable_lapack_bellatrix {
    module load atlas
    export LAPACK_INCLUDE_DIR=$ATLAS_INCLUDE
    export LAPACK_LIB_DIR=$ATLAS_LIBRARY
    echo "Using module atlas for lapack"
}

function install_lapack {

    echo "TODO! Use OpenBLAS, they include lapack"
    exit 1
    


    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    callFunc enable blas

    #BLAS_LIBRARIES=$LIBRARIES_PREFIX/lib/libgoto2_prescottp-r1.13.a
    
    # version
    BASE_NAME=lapack
    VERSION=$LAPACK_VERSION
    # check if already installed
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tgz
    if [ ! -d "$BASE_DIR_2" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://www.netlib.org/lapack/${PACKAGE}"
        fi
        tar -xf $PACKAGE
        # patch
        #cd $BASE_DIR_2
        #patch -p0 < ${LIBSETUP_BASE}/libs/gotoblas2_patch.diff
        #cd ..
    fi
    # configure
    BUILD_DIR=${BASE_DIR}_Build
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    # NOTE: forcing FORTRAN_COMPILER instead of MPI_FORTRAN_COMPILER otherwise it doesn't work!
    ${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
          -DCMAKE_INSTALL_PREFIX=$LIBRARIES_PREFIX \
          -DCMAKE_PREFIX_PATH=$LIBRARIES_PREFIX \
          -DCMAKE_SYSTEM_PREFIX_PATH=$LIBRARIES_PREFIX \
          -DCMAKE_FIND_ROOT_PATH=$LIBRARIES_PREFIX \
          -DCMAKE_C_COMPILER:STRING=$C_COMPILER \
          -DCMAKE_CXX_COMPILER:STRING=$CXX_COMPILER \
          -DCMAKE_Fortran_COMPILER:STRING=$FORTRAN_COMPILER \
          -DBLAS_LIBRARIES=$BLAS_LIBRARIES \
          -DBLAS_DIR=${BLAS_DIR} \
          -DUSE_OPTIMIZED_BLAS=YES \
          -DUSE_OPTIMIZED_LAPACK=YES \
          ../$BASE_DIR


    exit

    # build          
    make -j${MAKE_JOBS}
    # install
    make install
    cd ..

    
    # set as installed
    echo $VERSION > $INSTALLED
}

function uninstall_blas {
    libraries_uninstallLib "blas" $BLAS_VERSION
}

function clean_blas {
    libraries_cleanLib "blas" $BLAS_VERSION
}


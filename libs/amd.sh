#!/bin/bash

function enable_amd {
    # enable suitesparse
    callFunc enable suitesparse
    # use that one
    export AMD_BASE=${SUITESPARSE_BASE}
    export AMD_INCLUDE_DIR=${SUITESPARSE_INCLUDE_DIR}
    export AMD_LIB_DIR=${SUITESPARSE_LIB_DIR}
    export AMD_LIBRARIES=${SUITESPARSE_LIB_DIR}/libamd.a
    # check
    if [ ! -f "${AMD_LIBRARIES}" ]; then
        export AMD_LIBRARIES=${SUITESPARSE_LIB_DIR}/libamd.so
    fi
    if [ ! -f "${AMD_LIBRARIES}" ]; then
        echo "[Error] amd library not found in suitesparse"
        exit 1
    fi
    echo "Using custom amd from '${AMD_LIBRARIES}'"
}

function enable_amdOLD {
    # default dirs
    export AMD_INCLUDE_DIR=
    export AMD_LIB_DIR=
    # check if already installed
    BASE_NAME=amd
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        export AMD_INCLUDE_DIR=$LIBRARIES_PREFIX/include
        export AMD_LIB_DIR=$LIBRARIES_PREFIX/lib
        echo "Using custom ${BASE_NAME}"
    else
        # check SuiteSparse
        BASE_NAME2=SuiteSparse
        INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME2}_installed.txt
        if [ -f "$INSTALLED" ]; then
            # use custom one
            export SUITESPARSE_BASE_DIR=$LIBRARIES_PREFIX
            export SUITESPARSE_INCLUDE_DIR=$LIBRARIES_PREFIX/include
            export AMD_INCLUDE_DIR=${SUITESPARSE_INCLUDE_DIR}
            export SUITESPARSE_LIB_DIR=$LIBRARIES_PREFIX/lib
            export AMD_LIB_DIR=${SUITESPARSE_LIB_DIR}
            echo "Using custom ${BASE_NAME2} for ${BASE_NAME}"
        else
            echo "Using system installed ${BASE_NAME}"
        fi
    fi
}

function enable_amd_superb {
    callFunc enable suitesparse
    export AMD_INCLUDE_DIR=$SUITESPARSE_INCLUDE_DIR
    export AMD_LIB_DIR=$SUITESPARSE_LIB_DIR
    echo "Using SuperB amd"
}


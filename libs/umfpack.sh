#!/bin/bash

function enable_umfpack {
    # enable suitesparse
    callFunc enable suitesparse
    # use that one
    export UMFPACK_BASE=${SUITESPARSE_BASE}
    export UMFPACK_INCLUDE_DIR=${SUITESPARSE_INCLUDE_DIR}
    export UMFPACK_LIB_DIR=${SUITESPARSE_LIB_DIR}
    export UMFPACK_LIBRARIES=${UMFPACK_LIB_DIR}/libumfpack.a
    # check
    if [ ! -f "${UMFPACK_LIBRARIES}" ]; then
        export UMFPACK_LIBRARIES=${UMFPACK_LIB_DIR}/libumfpack.so
    fi
    if [ ! -f "${UMFPACK_LIBRARIES}" ]; then
        echo "[Error] umfpack library not found in suitesparse"
        exit 1
    fi
    echo "Using custom umfpack from '${UMFPACK_LIBRARIES}'"
}

function enable_umfpackOLD {
    # default dirs
    export UMFPACK_INCLUDE_DIR=
    export UMFPACK_LIB_DIR=
    # check if SuiteSparse is installed
    BASE_NAME=SuiteSparse
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        export SUITESPARSE_BASE_DIR=$LIBRARIES_PREFIX
        export SUITESPARSE_INCLUDE_DIR=$LIBRARIES_PREFIX/include
        export UMFPACK_INCLUDE_DIR=${SUITESPARSE_INCLUDE_DIR}
        export SUITESPARSE_LIB_DIR=$LIBRARIES_PREFIX/lib
        export UMFPACK_LIB_DIR=${SUITESPARSE_LIB_DIR}
        echo "Using custom ${BASE_NAME} for umfpack"
    else
        echo "Using system installed umfpack"
    fi
}

function enable_umfpack_superb {
    callFunc enable suitesparse
    export UMFPACK_INCLUDE_DIR=$SUITESPARSE_INCLUDE_DIR
    export UMFPACK_LIB_DIR=$SUITESPARSE_LIB_DIR
    echo "Using SuperB umfpack"
}


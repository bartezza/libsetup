#!/bin/bash

function enable_gcc {
    # nothing to do
    DONOTHING=
    echo "Using system installed C/C++ compiler ($USE_COMPILER)"
}

function enable_gcc_superb {
    if [ "$USE_COMPILER" = "gcc" ]; then
        source "/software/ENV/set_gcc-gnu-base.sh"
        echo "Using SuperB gcc"
    else
        echo "Compiler '$USE_COMPILER' not supported for this configuration"
        exit 1
    fi
}

function enable_gcc_bellatrix {
    if [ "$USE_COMPILER" = "gcc" ]; then
        module load gcc
        echo "Using module for gcc"
        #module load intel
        #export C_COMPILER="icc"
        #export CXX_COMPILER="icc"
        #echo "Using module for intel compilers"
    else
        echo "Compiler '$USE_COMPILER' not supported for this configuration"
        exit 1
    fi
}


#!/bin/bash

function enable_glm {
    # default enable
    libraries_enable "glm" ${GLM_VERSION}
    # export things
    export GLM_BASE_DIR=${CUR_BASE_DIR}
    export GLM_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    #export GLM_LIB_DIR=${CUR_LIB_DIR}
}

function install_glm {
    # version
    BASE_NAME=glm
    VERSION=$GLM_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.zip
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget -c "https://github.com/g-truc/glm/releases/download/${VERSION}/${PACKAGE}"
        fi
        unzip $PACKAGE
        mv glm ${BASE_DIR}
    fi
    
    # copy to install dir
    mkdir -p ${INSTALL_DIR}
    cp -r ${BASE_DIR}/* ${INSTALL_DIR}/
    
    # set as installed
    echo $VERSION > $INSTALLED
}


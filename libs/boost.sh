#!/bin/bash

function enable_boost {
    DONOTHING=
    echo "Using system installed boost"
}

function enable_boost_superb {
    source "/software/ENV/set_boost-gnu-base.sh"
    export BOOST_INCLUDE_DIR=$INSTDIR
    export BOOST_BASE_DIR=$INSTDIR
    echo "Using SuperB boost"
}

function enable_boost_bellatrix {
    module load boost
    export BOOST_INCLUDE_DIR=$BOOST_ROOT/include
    export BOOST_BASE_DIR=${BOOST_INCLUDE_DIR}
    echo "Using module for boost"
}


#!/bin/bash
#
# Depends on: gcc, openmpi, cmake

function enable_blas {
    if [ "$BLAS_TYPE" = "gotoblas" ]; then
        enable_blas_gotoblas
    elif [ "$BLAS_TYPE" = "openblas" ]; then
        enable_blas_openblas
    elif [ "$BLAS_TYPE" = "mkl" ]; then
        enable_blas_mkl
    else
        echo "Unsupported BLAS type '$BLAS_TYPE'"
        exit 1
    fi
}

function enable_blas_gotoblas {
    # default enable
    libraries_enable "gotoblas" ${GOTOBLAS_VERSION}
    # export things
    export BLAS_BASE_DIR=${CUR_BASE_DIR}
    export BLAS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export BLAS_LIB_DIR=${CUR_LIB_DIR}
    export BLAS_LIBRARIES=${CUR_LIB_DIR}/libgoto2.a
}

function enable_blas_openblas {
    # default enable
    libraries_enable "openblas" ${OPENBLAS_VERSION}
    # export things
    export BLAS_BASE_DIR=${CUR_BASE_DIR}
    export BLAS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    export BLAS_LIB_DIR=${CUR_LIB_DIR}
    export BLAS_LIBRARIES=${CUR_LIB_DIR}/libopenblas.a
}

function enable_blas_mkl {
    # dirs
    export BLAS_INCLUDE_DIR="$INTEL_MKL_BASE/include"
    export BLAS_LIB_DIR="$INTEL_MKL_BASE/lib/intel64"
    export BLAS_LIBRARIES="${BLAS_LIB_DIR}/libmkl_intel_lp64.a;$BLAS_LIB_DIR/libmkl_core.a"
    export BLAS_LIBRARIES="${BLAS_LIBRARIES};$BLAS_LIB_DIR/libmkl_gnu_thread.a;$INTEL_BASE/lib/intel64/libiomp5.a"
    echo "Using Intel BLAS (MKL)"
}

function enable_blas_bellatrix {
    module load atlas
    export BLAS_INCLUDE_DIR=$ATLAS_INCLUDE
    export BLAS_LIB_DIR=$ATLAS_LIBRARY
    echo "Using module atlas for blas"
}

function install_blas {
    if [ "$BLAS_TYPE" = "gotoblas" ]; then
        install_gotoblas
    elif [ "$BLAS_TYPE" = "openblas" ]; then
        install_openblas
    elif [ "$BLAS_TYPE" = "mkl" ]; then
        echo "Intel MKL should already be installed"
    else
        echo "Unsupported BLAS type '$BLAS_TYPE'"
        exit 1
    fi
}

function install_gotoblas {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    # version
    BASE_NAME=gotoblas
    BASE_NAME_2=GotoBLAS2
    VERSION=$GOTOBLAS2_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR_2=${BASE_NAME_2}-${VERSION}
    PACKAGE=${BASE_DIR_2}.tar.gz
    if [ ! -d "$BASE_DIR_2" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "https://www.tacc.utexas.edu/documents/1084364/1087496/${PACKAGE}"
        fi
        tar -xf $PACKAGE
        mv GotoBLAS2 $BASE_DIR_2
        # patch
        cd $BASE_DIR_2
        patch -p0 < ${LIBSETUP_BASE}/libs/gotoblas2_patch.diff
        cd ..
    fi
    # create build dir
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    # copy everything to build dir
    cp -r ../${BASE_DIR_2}/* .

    # make
    make -j${MAKE_JOBS} BINARY=64 DYNAMIC_ARCH=1 libs
    make -j${MAKE_JOBS} BINARY=64 DYNAMIC_ARCH=1 netlib
    
    # manual install
    mkdir -v -p ${INSTALL_DIR}
    mkdir -v -p ${INSTALL_DIR}/lib
    cp -v *.a ${INSTALL_DIR}/lib/
    
    # set as installed
    echo $VERSION > $INSTALLED
}

function install_openblas {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    # version
    BASE_NAME=openblas
    BASE_NAME_2=OpenBLAS
    VERSION=$OPENBLAS_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR_2=${BASE_NAME_2}-${VERSION}
    PACKAGE=v${VERSION}.tar.gz
    if [ ! -d "$BASE_DIR_2" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://github.com/xianyi/OpenBLAS/archive/${PACKAGE}"
        fi
        tar -xf $PACKAGE
        # patch
        #cd $BASE_DIR_2
        #patch -p0 < ${LIBSETUP_BASE}/libs/gotoblas2_patch.diff
        #cd ..
    fi
    # configure
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    if [ "$DO_NOT_CONFIGURE" != "1" ]; then
        ${CMAKE_BIN} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
              -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
              -DCMAKE_C_COMPILER:STRING=$C_COMPILER \
              -DCMAKE_CXX_COMPILER:STRING=$CXX_COMPILER \
              -DCMAKE_Fortran_COMPILER:STRING=$FORTRAN_COMPILER \
              ../$BASE_DIR_2
    fi
    # build
    make -j${MAKE_JOBS}
    # manual install
    mkdir -v -p ${INSTALL_DIR}
    mkdir -v -p ${INSTALL_DIR}/lib
    cp -v lib/*.a ${INSTALL_DIR}/lib/

    # set as installed
    echo $BASE_DIR_2 > $INSTALLED
}

function uninstall_blas {
    libraries_uninstallLib "blas" $BLAS_VERSION
}

function clean_blas {
    libraries_cleanLib "blas" $BLAS_VERSION
}


#!/bin/bash

function enable_umfpack {
    enable_suitesparse
    export UMFPACK_INCLUDE_DIR=$SUITESPARSE_INCLUDE_DIR
    export UMFPACK_LIB_DIR=$SUITSPARSE_LIB_DIR
}


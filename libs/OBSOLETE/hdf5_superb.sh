#!/bin/bash

function enable_hdf5 {
    source "/software/ENV/set_hdf5-parallel-gnu-base.sh"
    export HDF5_INCLUDE_DIR=$INSTDIR/include
    export HDF5_LIB_DIR=$INSTDIR/lib
}


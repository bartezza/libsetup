#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015-2016
#
# BLACS library installer
# Webpage: http://www.netlib.org/blacs/
#
# Version: 1.0
#
# 09/06/2016 - First version
#================================================================================#

function enable_blacs {
    if [ "$USE_COMPILER" = "intel" ]; then
        # dirs
        export BLACS_INCLUDE_DIR="$INTEL_MKL_BASE/include"
        export BLACS_LIB_DIR="$INTEL_MKL_BASE/lib/intel64"
        if [ "$USE_COMPILER_MPI" = "intel-mpi" ]; then
            export BLACS_LIBRARIES="${BLACS_LIB_DIR}/libmkl_blacs_intelmpi_lp64.a"
        elif [ "$USE_COMPILER_MPI" = "intel-openmpi" ]; then
            export BLACS_LIBRARIES="${BLACS_LIB_DIR}/libmkl_blacs_openmpi_lp64.a"
        fi
        # check
        if [ ! -f "${BLACS_LIBRARIES}" ]; then
            echo "[Error] BLACS library not found"
            exit 1
        fi
        echo "Using Intel BLACS"
    else
        # default enable
        libraries_enable "blacs" ${BLACS_VERSION}
        # export things
        export BLACS_BASE_DIR=${CUR_BASE_DIR}
        export BLACS_INCLUDE_DIR=${CUR_INCLUDE_DIR}
        export BLACS_LIB_DIR=${CUR_LIB_DIR}
        BLACS_LIBRARIES="${BLACS_LIB_DIR}/blacs_MPI-LINUX-0.a"
        BLACS_LIBRARIES="${BLACS_LIBRARIES};${BLACS_LIB_DIR}/blacsCinit_MPI-LINUX-0.a"
        BLACS_LIBRARIES="${BLACS_LIBRARIES};${BLACS_LIB_DIR}/blacsF77init_MPI-LINUX-0.a"
        export BLACS_LIBRARIES
    fi
}

#function enable_blacs_superb {
#    source "/software/ENV/set_blacs-gnu-base.sh"
#    export BLACS_BASE=$INSTDIR
#    export BLACS_INCLUDE_DIR=$INSTDIR/include
#    export BLACS_LIB_DIR=$INSTDIR/lib
#    echo "Using SuperB blacs"
#}

#function enable_blacs_bellatrixNO {
#    module load blacs
#    export BLACS_BASE=$BLACS_ROOT
#    #export BLACS_INCLUDE=$BLACS_INCLUDE
#    export BLACS_LIB_DIR=$BLACS_LIBRARY
#    echo "Using module for blacs"
#}

function install_blacs {
    # required libraries
    callFunc enable gcc
    callFunc enable openmpi
    callFunc enable cmake
    callFunc enable blas
    # version
    BASE_NAME=blacs
    VERSION=$BLACS_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=BLACS
    PACKAGE=mpiblacs.tgz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "http://www.netlib.org/blacs/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    
    # setup build dir
    BUILD_DIR=$( getBuildDir ${BASE_NAME} ${VERSION} )
    if [ "$OPERATION" = "reinstall" ]; then
        rm -rf $BUILD_DIR
    fi
    mkdir -p $BUILD_DIR
    # copy everything
    cp -r ${BASE_DIR}/* ${BUILD_DIR}
    
    # copy makefile
    DEST_MAKEFILE=${BUILD_DIR}/Bmake.inc
    cp -v ${LIBSETUP_BASE}/libs/${BASE_NAME}_Bmake.MPI-LINUX ${DEST_MAKEFILE}
    # apply template
    prepareSedScript
    #BLACSDIR=$( pwd )/${BASE_DIR}
    echo "s/\[BTOPdir\]/$(escapeForSed $BUILD_DIR)/g" >> $SED_TMP
    echo "s/\[MPI_INCLUDE_DIR\]/$(escapeForSed $MPI_INCLUDE_DIR)/g" >> $SED_TMP
    echo "s/\[MPI_LIB_DIR\]/$(escapeForSed $MPI_LIB_DIR)/g" >> $SED_TMP
    echo "s/\[MPI_LIBRARIES\]/$(escapeForSed $MPI_LIBRARIES)/g" >> $SED_TMP
    applySedScript "${DEST_MAKEFILE}"
    
    # build
    cd ${BUILD_DIR}
    make -j${MAKE_JOBS} mpi

    # manual install
    mkdir -p ${INSTALL_DIR}
    mkdir -p ${INSTALL_DIR}/include
    cp -v SRC/MPI/*.h ${INSTALL_DIR}/include/
    mkdir -p ${INSTALL_DIR}/lib
    cp -v LIB/*.a ${INSTALL_DIR}/lib/
    cp -v LIB/blacs_MPI-LINUX-0.a ${INSTALL_DIR}/lib/libblacs.a
    cd ..
    
    # set as installed
    echo $VERSION > $INSTALLED
}

function uninstall_blacs {
    libraries_uninstallLib "blacs" $BLACS_VERSION
}

function clean_blacs {
    libraries_cleanLib "blacs" $BLACS_VERSION
}


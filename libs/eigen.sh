#!/bin/bash

function enable_eigen {
    # default enable
    libraries_enable "eigen" ${EIGEN_VERSION}
    # export things
    export EIGEN_BASE_DIR=${CUR_BASE_DIR}
    export EIGEN_INCLUDE_DIR=${CUR_INCLUDE_DIR}
    #export EIGEN_LIB_DIR=${CUR_LIB_DIR}
}

function install_eigen {
    # version
    BASE_NAME=eigen
    VERSION=$EIGEN_VERSION
    # check if already installed
    INSTALL_DIR=$( getInstallDir ${BASE_NAME} ${VERSION} )
    INSTALLED=$INSTALL_DIR/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.bz2
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget -c "http://bitbucket.org/eigen/eigen/get/${VERSION}.tar.bz2" -O ${PACKAGE}
        fi
        tar -xf $PACKAGE
        mv eigen-eigen-* $BASE_DIR
    fi
    if [ "$DO_DOWNLOAD_ONLY" = "1" ]; then
        return
    fi
    
    # copy to install dir
    mkdir -p ${INSTALL_DIR}
    mkdir -p ${INSTALL_DIR}/include
    cp -r ${BASE_DIR}/Eigen ${INSTALL_DIR}/include/
    
    # set as installed
    echo $VERSION > $INSTALLED
}


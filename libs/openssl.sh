#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# OpenSSL library installer
# Webpage: https://www.openssl.org/source/
#
# Version: 1.0
#
# 26/10/2015 - First version
#================================================================================#

function enable_openssl {
    # default dirs
    OPENSSL_INCLUDE_DIR=
    OPENSSL_LIB_DIR=
    # check if already installed
    BASE_NAME=openssl
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ -f "$INSTALLED" ]; then
        # use custom one
        OPENSSL_INCLUDE_DIR=$LIBRARIES_PREFIX/include
        OPENSSL_LIB_DIR=$LIBRARIES_PREFIX/lib
        echo "Using custom ${BASE_NAME}"
    else
        echo "Using system installed ${BASE_NAME}"
    fi
}

#function enable_openssl_superb {
#    source "/software/ENV/set_openssl-gnu-base.sh"
#    export OPENSSL_BASE=$INSTDIR
#    export OPENSSL_INCLUDE_DIR=$INSTDIR/include
#    export OPENSSL_LIB_DIR=$INSTDIR/lib
#    echo "Using SuperB openssl"
#}

function install_openssl {
    # version
    BASE_NAME=openssl
    VERSION=$OPENSSL_VERSION
    # check if already installed
    INSTALLED=$LIBRARIES_PREFIX/${BASE_NAME}_installed.txt
    if [ "$OPERATION" != "reinstall" ]; then
        if [ -f "$INSTALLED" ]; then
            echo "${BASE_NAME} already installed (version `cat $INSTALLED`)"
            return
        fi
    fi
    # download and extract
    BASE_DIR=${BASE_NAME}-${VERSION}
    PACKAGE=${BASE_DIR}.tar.gz
    if [ ! -d "$BASE_DIR" ]; then
        if [ ! -f "$PACKAGE" ]; then
            wget "https://www.openssl.org/source/${PACKAGE}"
        fi
        tar -xf $PACKAGE
    fi
    # configure
    #BUILD_DIR=${BASE_DIR}_Build
    #if [ "$OPERATION" = "reinstall" ]; then
    #    rm -rf $BUILD_DIR
    #fi
    #mkdir -p $BUILD_DIR
    cd $BASE_DIR
    ./Configure --prefix=$LIBRARIES_PREFIX linux-x86_64 shared
    make -j8
    make install
    cd ..
    # set as installed
    echo $VERSION > $INSTALLED
}


#!/bin/bash

set -e

OPTS="--download"

libsetup $OPTS install cmake

echo
libsetup $OPTS install glm

echo
libsetup $OPTS install eigen

echo
libsetup $OPTS install blas

echo
libsetup $OPTS install blacs

echo
libsetup $OPTS install scalapack

echo
libsetup $OPTS install metis

echo
libsetup $OPTS install parmetis

echo
libsetup $OPTS install suitesparse

echo
libsetup $OPTS install scotch

echo
libsetup $OPTS install mumps

echo
libsetup $OPTS install trilinos_isoglib


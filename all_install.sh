#!/bin/bash

# Example script to install the useful stuff on a local debian computer

sudo apt-get install gfortran libopenmpi-dev

libsetup install blas
libsetup install metis
libsetup install parmetis
libsetup install scalapack
libsetup install hdf5
libsetup install suitesparse
libsetup install scotch
libsetup install blacs
libsetup install mumps
libsetup install trilinos_isoglib


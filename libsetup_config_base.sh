#!/bin/bash
#================================================================================#
# libsetup: script to automatize the setup from sources of libraries.
# Written by Andrea Bartezzaghi, 2015
#
# Example of configuration script.
# Copy this to "libsetup_config.sh" and customize it.
#
# Version: 1.0
#
# 07/10/2015 - First version
#================================================================================#

function pause {
    #echo "Press any key to continue"; read
    echo
}

export LIBRARIES_BASE=/usr/scratch/bartezza/Libraries
export LIBRARIES_PREFIX=$LIBRARIES_BASE/Libraries
export LIBRARIES_PREFIX_DEBUG=$LIBRARIES_BASE/LibrariesDebug

if [ "$INTEL_BASE" = "" ]; then
    export INTEL_BASE="/opt/intel"
fi

if [ "$USE_COMPILER" = "" ]; then
    export USE_COMPILER="gcc"
fi
if [ "$USE_COMPILER_MPI" = "" ]; then
    export USE_COMPILER_MPI="gcc-openmpi"
fi

if [ "$USE_COMPILER" = "gcc" ]; then
    export C_COMPILER="gcc"
    export CXX_COMPILER="g++"
    export FORTRAN_COMPILER="gfortran"
    export COMPILER_SUFFIX="Gcc"
    
    if [ "$USE_COMPILER_MPI" = "gcc-intelmpi" ]; then
        # intel MPI
        export MPI_BASE="${INTEL_BASE}/compilers_and_libraries/linux/mpi/intel64"
        export MPI_INCLUDE_DIR="${MPI_BASE}/include"
        export PATH="$MPI_BASE/bin:$PATH"
        export MPI_C_COMPILER="mpigcc"
        export MPI_CXX_COMPILER="mpigxx"
        export MPI_FORTRAN_COMPILER="mpifc"
        export MPI_EXEC="${MPI_BASE}/bin/mpiexec"
        export COMPILER_SUFFIX_MPI="GccIntelMpi"
    else
        # system-wide compiler
        export MPI_INCLUDE_DIR="/usr/include/mpi"
        export MPI_C_COMPILER="mpicc"
        export MPI_CXX_COMPILER="mpic++"  # on MacOS: "mpic++-mpich-mp"
        export MPI_FORTRAN_COMPILER="mpif90"
        export MPI_EXEC="mpiexec"
        export COMPILER_SUFFIX_MPI="GccMpi"
    fi
fi

if [ "$USE_COMPILER" = "intel" ]; then
    
    export MPI_BASE="${INTEL_BASE}/compilers_and_libraries/linux/mpi/intel64"
    
    export INTEL_MKL_BASE="$INTEL_BASE/mkl"
    
    export C_COMPILER="icc"
    export CXX_COMPILER="icc"
    export FORTRAN_COMPILER="ifort"
    
    export MPI_C_COMPILER="mpiicc"
    export MPI_CXX_COMPILER="mpiicpc"
    export MPI_FORTRAN_COMPILER="mpiifort"
    export MPI_EXEC="${MPI_BASE}/bin/mpiexec"
    
    export COMPILER_SUFFIX="Intel"
    
    export USE_COMPILER_MPI="intel-mpi"
    export MPI_INCLUDE_DIR="${MPI_BASE}/include"
    
    # source the path of the compilers
    export ICC_PATH="${INTEL_BASE}/compilers_and_libraries/linux/bin/intel64"
    export MPIICC_PATH="${MPI_BASE}/bin"
    export PATH="$MPIICC_PATH:$ICC_PATH:$PATH"
    export LD_LIBRARY_PATH="${INTEL_BASE}/compilers_and_libraries/linux/lib/intel64_lin"
    export COMPILER_SUFFIX_MPI="IntelMpi"
fi

export MAKE_JOBS=8

export BUILD_TYPE="Release"

# create a common install prefix or separated
export SEPARATE_INSTALL=1

# machine config
# builtin configs: superb
#export MACHINE_CONFIG="superb"

# example of custom config
#export metis_CONFIG=example

# library versions
export METIS_VERSION=5.1.0

export PARMETIS_VERSION=4.0.3

export HDF5_VERSION=1.8.17

export SUITESPARSE_VERSION=4.5.6 # was 4.4.5

export TRILINOS_ISOGLIB_VERSION=12.6.1
export TRILINOS_USE_MUMPS=1

export CMAKE_VERSION=3.4.0-rc1
export CMAKE_VERSION_MAJOR=3.4

export CURL_VERSION=7.45.0

export OPENSSL_VERSION=1.0.2d

export MUMPS_VERSION=5.0.1

export SCOTCH_VERSION=6.0.4

export SCALAPACK_VERSION=2.0.2

export GOTOBLAS2_VERSION=1.13
export OPENBLAS_VERSION=0.2.20

if [ "$USE_COMPILER" = "intel" ]; then
    export BLAS_TYPE="mkl"
else
    export BLAS_TYPE="openblas"
fi

export BLACS_VERSION=1.1

export LAPACK_VERSION=3.6.0

if [ "$USE_COMPILER" = "intel" ]; then
    export LAPACK_TYPE="mkl"
else
    export LAPACK_TYPE="openblas"
fi

export GLM_VERSION=0.9.8.2

export EIGEN_VERSION=3.3.1

export LUA_VERSION=5.3.4

